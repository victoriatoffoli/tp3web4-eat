'use strict';

var express = require('express');
var app = express();

//Permet de récupérer du JSON dans le corps de la requête
var bodyParser = require('body-parser');
app.use(bodyParser.json());


// Paramètres de configuration généraux.
var config = require('./config');

// Module pour JWT.
var jwt = require('jsonwebtoken');

var hateoasLinker = require('express-hateoas-links');

app.use(hateoasLinker)

// Ajout d'une variable globale à l'application.
app.set('jwt-secret', config.secret);

//Importation de modèle Usager
var UsagerModel = require('./models/usagerModels').UsagerModel;


// Route pour l'authentification (connexion).
app.post('/connexions', function (req, res) {

    var pseudo = "";
    var motDePasse = "";

    try {
        pseudo = req.body.pseudo;
        motDePasse = req.body.motDePasse;
        
    } catch (error) {

        res.status(400).end();        
    }

    // Vérification des informations d'authentification.
    UsagerModel.find({pseudo: pseudo, motDePasse: motDePasse}, function(err, ressources) {
        if(err) throw err;
        if(ressources.length === 1){

            console.log('Création du token pour ' + req.body.pseudo);

             // Créer le jeton JWT car les informations d'authentification sont valides.
            var payload = {
                user: ressources[0].id,
                access: "user"
            };
            var jwtToken = jwt.sign(payload, app.get('jwt-secret'), {
                expiresIn: 86400
                
            });
            res.status(201).json({
                "token": jwtToken
            });
                       
        }else {

            res.status(400).end();
            
        }
    });
 
});

//importe notre routeur du fichier api.js
var routerApiLivreurs = require('./routes/apiLivreurs.js');
var routerApiPlats = require('./routes/apiPlats.js');
var routerApiUsagers = require('./routes/apiUsagers.js');
//indique à notre app d'utiliser le routeur pour toutes les requêtes à partir de la racine du site web
app.use('/', routerApiLivreurs);
app.use('/', routerApiPlats);
app.use('/', routerApiUsagers);

// Gestion de l'erreur 404.
app.all('*', function (req, res) {
    res.setHeader('Content-Type', 'text/plain; charset=utf-8');
    res.status(404).send('Erreur 404 : Ressource inexistante !');
});

// Démarrage du serveur.
app.listen(8090, function () {
    console.log('Serveur sur port ' + this.address().port);
});