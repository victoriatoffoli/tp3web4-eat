'use strict';

var express = require('express');

var routerApiLivreurs = express.Router();
var url_base = "http://localhost:8090";
//ORM Mongoose
var mongoose = require('mongoose');
// Connexion à MongoDB avec Mongoose
mongoose.connect('mongodb://localhost:27017/web4Tp3', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    poolSize: 10
});

//Importation de modèle Livreur
var LivreurModel = require('../models/livreurModels').LivreurModel;

var jwt = require('jsonwebtoken');

/**
 * Permet de vérifier si l'utilisateur est authentifié à l'aide d'un jeton JWT.
 * @param {Object} req Requête HTTP.
 * @param {Function} callback Fonction de callback.
 */
function verifierAuthentification(req, callback) {
    // Récupération du jeton JWT dans l'en-tête HTTP "Authorization".
    var auth = req.headers.authorization;
    if (!auth) {
        // Pas de jeton donc pas connecté.
        callback(false, null);
    } else {
        // Structure de l'en-tête "Authorization" : "Bearer jeton-jwt"
        var authArray = auth.split(' ');
        if (authArray.length !== 2) {
            // Mauvaise structure pour l'en-tête "Authorization".
            callback(false, null);
        } else {
            // Le jeton est après l'espace suivant "Bearer".
            var jetonEndode = authArray[1];
            // Vérification du jeton.
            jwt.verify(jetonEndode, req.app.get('jwt-secret'), function (err, jetonDecode) {
                if (err) {
                    // Jeton invalide.
                    callback(false, null);
                } else {
                    // Jeton valide.
                    callback(true, jetonDecode);
                }
            });
        }
    }
}

// Ajout d'un middleware qui intercepte toutes les requêtes.
routerApiLivreurs.use(function (req, res, next) {
    // Vérification de l'authorisation.
    verifierAuthentification(req, function (estAuthentifie, jetonDecode) {
        if (!estAuthentifie) {
            // Utilisateur NON authentifié.
            res.status(401).end();
        } else {
            // Utilisateur authentifié.
            // Sauvegarde du jeton décodé dans la requête pour usage ultérieur.
            req.jeton = jetonDecode;
            // Poursuite du traitement de la requête.
            next();
        }
    });
});

// Création d'un livreur
// =======================================================
routerApiLivreurs.route('/livreurs')
    .post(function (req, res) {
        //on doit créer un livreur
        console.log('Création d\'un livreur ');
        //création du modèle à partir du body de la requête
        var livreur = new LivreurModel(req.body);
        //on sauvegarde dans la BD
        livreur.save(function (err) {
            if (err) throw err;

            res.setHeader("Location", "http://localhost:8090/livreurs/" + livreur.id);
            res.status(201).json(livreur);
        });
    });

// Route de consultation d'un livreur
// =======================================================
routerApiLivreurs.route('/livreurs/:livreur_id')
    //Consultation d'un livreur
    .get(function (req, res) {
        console.log('consultation du livreur no.' + req.params.livreur_id);
        LivreurModel.findById(req.params.livreur_id, function (err, livreur) {
            if (err) throw err;
            if (livreur)
                res.json(livreur);
            else
                res.status(404).end();
        });
    });


// Route pour supprimer un livreur
// =======================================================
routerApiLivreurs.route('/livreurs/:livreur_id')
    //destruction du livreur
    .delete(function (req, res) {
        console.log('suppresion du livreur no.' + req.params.livreur_id);
        LivreurModel.findByIdAndDelete(req.params.livreur_id, function (err) {
            if (err) throw err;
            res.status(204).end();
        });
    });


//exportation de routerAPI
module.exports = routerApiLivreurs;