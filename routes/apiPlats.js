'use strict';

var express = require('express');

var routerApiPlats = express.Router();
var url_base = "http://localhost:8090";
//ORM Mongoose
var mongoose = require('mongoose');
// Connexion à MongoDB avec Mongoose
mongoose.connect('mongodb://localhost:27017/web4Tp3', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    poolSize: 10
});

//Importation de modèle Plat
var PlatModel = require('../models/platModels').PlatModel;

var jwt = require('jsonwebtoken');

/**
 * Permet de vérifier si l'utilisateur est authentifié à l'aide d'un jeton JWT.
 * @param {Object} req Requête HTTP.
 * @param {Function} callback Fonction de callback.
 */
function verifierAuthentification(req, callback) {
    // Récupération du jeton JWT dans l'en-tête HTTP "Authorization".
    var auth = req.headers.authorization;
    if (!auth) {
        // Pas de jeton donc pas connecté.
        callback(false, null);
    } else {
        // Structure de l'en-tête "Authorization" : "Bearer jeton-jwt"
        var authArray = auth.split(' ');
        if (authArray.length !== 2) {
            // Mauvaise structure pour l'en-tête "Authorization".
            callback(false, null);
        } else {
            // Le jeton est après l'espace suivant "Bearer".
            var jetonEndode = authArray[1];
            // Vérification du jeton.
            jwt.verify(jetonEndode, req.app.get('jwt-secret'), function (err, jetonDecode) {
                if (err) {
                    // Jeton invalide.
                    callback(false, null);
                } else {
                    // Jeton valide.
                    callback(true, jetonDecode);
                }
            });
        }
    }
}

// Ajout d'un middleware qui intercepte toutes les requêtes.
routerApiPlats.use(function (req, res, next) {
    // Vérification de l'authorisation.
    verifierAuthentification(req, function (estAuthentifie, jetonDecode) {
        if (!estAuthentifie) {
            // Utilisateur NON authentifié.
            res.status(401).end();
        } else {
            // Utilisateur authentifié.
            // Sauvegarde du jeton décodé dans la requête pour usage ultérieur.
            req.jeton = jetonDecode;
            // Poursuite du traitement de la requête.
            next();
        }
    });
});




// Création d'un plat
// =======================================================
routerApiPlats.route('/plats')
    .post(function (req, res) {
        //on doit créer un plat
        console.log('Création d\'un plat ');
        //création du modèle à partir du body de la requête
        var plat = new PlatModel(req.body);
        //on sauvegarde dans la BD
        plat.save(function (err) {
            if (err) throw err;

            res.setHeader("Location", "http://localhost:8090/livreurs/" + plat.id);
            res.status(201).json(plat, [
                { rel: "self", method: "GET", href: "http://localhost:8090/plats/"+plat._id.toString() },
                { rel: "delete", method: "DELETE", href: "http://localhost:8090/plats/"+plat._id.toString()}
            ]);
        });
    });

// Route de consultation d'un plat
// =======================================================
routerApiPlats.route('/plats/:plat_id')
    //Consultation d'un plat
    .get(function (req, res) {
        console.log('consultation du plat no.' + req.params.plat_id);
        PlatModel.findById(req.params.plat_id, function (err, plat) {
            if (err) throw err;
            if (plat)
                //en passant le tableau de links en paramètre, express-hateoas-links s'occupera de créer le champs 'Links' dans la réponse
                res.status(200).json(plat, [
                    { rel: "self", method: "GET", href: "http://localhost:8090/plats/"+plat._id.toString() },
                    { rel: "delete", method: "DELETE", href: "http://localhost:8090/plats/"+plat._id.toString()}
                    ]);
            else
                res.status(404).end();
        });
    });

// Route de consultation des plats
// =======================================================
routerApiPlats.route('/plats')
    //Consultation de tous les  plats
    .get(function (req, res) {
        console.log('consultation de tous les plats');
        PlatModel.find({}, function (err, plats) {
            var resBody = [];
            
            plats.forEach(plat => {
                //C'Est ici qu'on inclut les hyperliens que l'on souhaite joindre à chaque élément de la collection
                var links =[
                    { rel: "self", method: "GET", href: "http://localhost:8090/plats/"+plat._id.toString() },
                    { rel: "delete", method: "DELETE", href: "http://localhost:8090/plats/"+plat._id.toString()}
                ];
                var platToJson = plat.toJSON();
                var platsAvecLink = {
                    plat : platToJson,
                    links : links
                };
                resBody.push(platsAvecLink);
            });
            if (err) throw err;
            res.json(resBody);


        });
    });

// Route pour supprimer un plat
// =======================================================
routerApiPlats.route('/plats/:plat_id')
    //destruction du plat
    .delete(function (req, res) {
        console.log('suppresion du plat no.' + req.params.plat_id);
        PlatModel.findByIdAndDelete(req.params.plat_id, function (err) {
            if (err) throw err;
            res.status(204).end();
        });
    });


//exportation de routerAPI
module.exports = routerApiPlats;