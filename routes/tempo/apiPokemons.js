'use strict';

var express = require('express');

var routerApiPokemon = express.Router();
var url_base = "http://localhost:8090";
//ORM Mongoose
var mongoose = require('mongoose');
// Connexion à MongoDB avec Mongoose
mongoose.connect('mongodb://localhost:27017/pokecenter', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    poolSize: 10
});

//Importation de modèle Pokemon
var PokemonModel = require('../models/pokemonModels').PokemonModel;

//exécuté à chaque requête à l'APi
routerApiPokemon.use(function (req, res, next) {
    //Log les requêtes
    console.log(req.method, req.url);
    //permet de poursuivre le traitement de la requête
    next();
});

// Route de création d'un pokemon
// =======================================================
routerApiPokemon.route('/pokemon')
    .post(function (req, res) {
        console.log('création du pokemon ');
        //création du modèle à partir du body de la requête
        var nouveauPokemon = new PokemonModel(req.body);
        //on sauvegarde dans la BD
        nouveauPokemon.save(function (err) {
            if (err) throw err;
            //si la sauvegarde fonctionne, on retourne 201 et on met le nouveau pokemon dans le body de la réponse
            res.setHeader("Location", "http://localhost:8090/pokemons/" + nouveauPokemon.id);
            res.status(201).json(nouveauPokemon);
        });
    });

// Route de consultation des pokemons
// =======================================================
routerApiPokemon.route('/pokemons')
    //Consultation de tous les  pokemons
    //VOUS DEVEZ GÉREZ LES PARAMÈTRES ?disponible=true ou =false
    .get(function (req, res) {
        console.log('consultation de tous les pokemons');
        console.log(req.query.disponible)

        if (req.query.disponible === "true") {

            PokemonModel.find({
                emprunt: {
                    $exists: false
                }
            }, function (err, pokemons) {
                if (err) throw err;
                res.json(pokemons);
            });
        } else if (req.query.disponible === "false") {

            PokemonModel.find({
                emprunt: {
                    $exists: true
                }
            }, function (err, pokemons) {
                if (err) throw err;
                res.json(pokemons);
            });
        } else {

            PokemonModel.find({}, function (err, pokemons) {
                if (err) throw err;
                res.json(pokemons);
            });
        }
    });


// Route de consultation d'un pokemon
// =======================================================
routerApiPokemon.route('/pokemons/:pokemon_id')
    //Consultation d'un pokemon
    .get(function (req, res) {
        console.log('consultation du pokemon no.' + req.params.pokemon_id);
        PokemonModel.findById(req.params.pokemon_id, function (err, pokemon) {
            if (err) throw err;
            if (pokemon)
                res.json(pokemon);
            else
                res.status(404).end();
        });
    });



// Modification d'un pokemon
// =======================================================
routerApiPokemon.route('/pokemons/:pokemon_id')
    .put(function (req, res) {
        PokemonModel.findById(req.params.pokemon_id, function (err, pokemon) {
            if (err) throw err;
            if (pokemon === null) {
                //on doit créer un pokemon
                console.log('création du pokemon ');
                //création du modèle à partir du body de la requête
                pokemon = new PokemonModel(req.body);
                //on sauvegarde dans la BD
                pokemon.save(function (err) {
                    if (err) throw err;
                    res.status(201).json(pokemon);
                });
            } else {

                console.log('modification du pokemon no.' + req.params.pokemon_id);
                PokemonModel.findByIdAndUpdate(req.params.pokemon_id, req.body, {
                    new: true,
                    runValidators: true
                }, function (err, pokemon) {
                    if (err) throw err;
                    res.json(pokemon);

                });
            }
        });
    });


// Route pour supprimer un pokemon
// =======================================================
routerApiPokemon.route('/pokemons/:pokemon_id')
    //Consultation d'un pokemon
    .delete(function (req, res) {
        console.log('suppresion du pokemon no.' + req.params.pokemon_id);
        PokemonModel.findByIdAndDelete(req.params.pokemon_id, function (err) {
            if (err) throw err;
            res.status(204).end();
        });
    });

//Route autres (all)
//=============================================================
routerApiPokemon.route('/pokemons')
    .all(function (req, res) {
        res.status(405).end();
    });

routerApiPokemon.route('/pokemons/:pokemon_id')
    .all(function (req, res) {
        res.status(405).end();
    });

//exportation de routerAPI
module.exports = routerApiPokemon;