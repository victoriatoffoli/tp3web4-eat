'use strict';

var express = require('express');

var routerApiDresseur = express.Router();
var url_base = "http://localhost:8090";
//ORM Mongoose
var mongoose = require('mongoose');
// Connexion à MongoDB avec Mongoose
mongoose.connect('mongodb://localhost:27017/pokecenter', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    poolSize: 10
});

//Importation de modèle Dresseur
var DresseurModel = require('../models/dresseurModels').DresseurModel;
var PokemonModel = require('../models/pokemonModels').PokemonModel;

// Création d'un dresseur
// =======================================================
routerApiDresseur.route('/dresseur')
    .post(function (req, res) {
        //on doit créer un dresseur
        console.log('création du dresseur ');
        //création du modèle à partir du body de la requête
        var dresseur = new DresseurModel(req.body);
        //on sauvegarde dans la BD
        dresseur.save(function (err) {
            if (err) throw err;

            res.setHeader("Location", "http://localhost:8090/dresseurs/" + dresseur.id);
            res.status(201).json(dresseur);
        });
    });

// Route de consultation des dresseurs
// =======================================================
routerApiDresseur.route('/dresseurs')
    //Consultation de tous les  dresseurs
    .get(function (req, res) {
        console.log('consultation de tous les dresseurs');
        DresseurModel.find({}, function (err, dresseurs) {
            if (err) throw err;
            res.json(dresseurs);


        });
    });

// Route de consultation d'un dresseur
// =======================================================
routerApiDresseur.route('/dresseurs/:dresseur_id')
    //Consultation d'un dresseur
    .get(function (req, res) {
        console.log('consultation du dresseur no.' + req.params.dresseur_id);
        DresseurModel.findById(req.params.dresseur_id, function (err, dresseur) {
            if (err) throw err;
            if (dresseur)
                res.json(dresseur);
            else
                res.status(404).end();
        });
    });


// Route pour supprimer un dresseur
// =======================================================
routerApiDresseur.route('/dresseurs/:dresseur_id')
    //Consultation d'un dresseur
    .delete(function (req, res) {
        console.log('suppresion du dresseur no.' + req.params.dresseur_id);
        DresseurModel.findByIdAndDelete(req.params.dresseur_id, function (err) {
            if (err) throw err;
            //Delete des pokemons
            PokemonModel.find({
                'emprunt.dresseurId': req.params.dresseur_id
            }, function (err, pokemons) {
                if (err) throw err;

                if (pokemons) {
                    pokemons.map(pokemon => {
                        pokemon.emprunt = undefined;
                        pokemon.save();
                    });

                }
                //================


                res.status(204).end();
            });
        });
    });

// Modification d'un dresseur
// =======================================================
routerApiDresseur.route('/dresseurs/:dresseur_id')
    .put(function (req, res) {
        DresseurModel.findById(req.params.dresseur_id, function (err, dresseur) {
            if (err) throw err;
            if (dresseur === null) {
                //on doit créer un dresseur
                console.log('création du dresseur ');
                //création du modèle à partir du body de la requête
                dresseur = new DresseurModel(req.body);
                //on sauvegarde dans la BD
                dresseur.save(function (err) {
                    if (err) throw err;
                    res.status(201).json(dresseur);
                });
            } else {

                console.log('modification du dresseur no.' + req.params.dresseur_id);
                DresseurModel.findByIdAndUpdate(req.params.dresseur_id, req.body, {
                    new: true,
                    runValidators: true
                }, function (err, dresseur) {
                    if (err) throw err;
                    res.json(dresseur);

                });
            }
        });
    });


// Route de consultation des pokemons d'un dresseur
// =======================================================
routerApiDresseur.route('/dresseurs/:dresseur_id/pokemons')
    .get(function (req, res) {
        console.log('consultation des pokemons du dresseur no.' + req.params.dresseur_id);
        PokemonModel.find({
            'emprunt.dresseurId': req.params.dresseur_id
        }, function (err, pokemons) {
            if (err) throw err;
            res.status(200).json(pokemons);
        });
    });


//Route autres (all)
//=============================================================
routerApiDresseur.route('/dresseur')
    .all(function (req, res) {
        res.status(405).end();
    });

routerApiDresseur.route('/dresseurs/:dresseur_id')
    .all(function (req, res) {
        res.status(405).end();
    });

routerApiDresseur.route('/dresseurs/:dresseur_id/pokemons')
    .all(function (req, res) {
        res.status(405).end();
    });

//exportation de routerAPI
module.exports = routerApiDresseur;