'use strict';

var express = require('express');

var routerApiEmprunt = express.Router();

//ORM Mongoose
var mongoose = require('mongoose');
// Connexion à MongoDB avec Mongoose
mongoose.connect('mongodb://localhost:27017/pokecenter', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    poolSize: 10
});

//Importation de modèle Pokemon
var PokemonModel = require('../models/pokemonModels').PokemonModel;


// pour UN emprunt
// =======================================================
routerApiEmprunt.route('/pokemons/:pokemon_id/emprunt')

    //Consultation d'un emprunt
    .get(function (req, res) {
        console.log('consultation de l\'emprunt du pokemon no: ' + req.params.pokemon_id);

        //est-ce que le pokemon existe : 
        PokemonModel.findById(req.params.pokemon_id, function (err, pokemon) {
            if (err) throw err;
            if (pokemon && pokemon.emprunt) {

                if (pokemon.emprunt === null) {
                    res.status(404).end();
                } else
                    res.json(pokemon.emprunt);
            } else
                res.status(404).end();
        });
    });




// Modification d'un dresseur
// =======================================================
routerApiEmprunt.route('/pokemons/:pokemon_id/emprunt')
    .put(function (req, res) {
        PokemonModel.findById(req.params.pokemon_id, function (err, pokemon) {
            if (err) throw err;
            if (pokemon === null) {
                res.status(404).end();
            } else if (pokemon.emprunt) {
                console.log('modification de l\'emprunt du pokemon no.' + req.params.pokemon_id);
                PokemonModel.findByIdAndUpdate(req.params.pokemon_id, {
                    emprunt: req.body
                }, {
                    new: true,
                    runValidators: true
                }, function (err, pokemon) {
                    if (err) throw err;
                    res.status(200).json(pokemon.emprunt);

                });
            } else {

                console.log('ajout de l\'emprunt du pokemon no.' + req.params.pokemon_id);
                PokemonModel.findByIdAndUpdate(req.params.pokemon_id, {
                    emprunt: req.body
                }, {
                    new: true,
                    runValidators: true
                }, function (err, pokemon) {
                    if (err) throw err;

                    res.status(201).json(pokemon.emprunt);

                });


            }
        });
    });

// Route pour supprimer un dresseur
// =======================================================
routerApiEmprunt.route('/pokemons/:pokemon_id/emprunt')
    //Consultation d'un dresseur
    .delete(function (req, res) {
        console.log('suppresion de l\'emprunt du pokemon no.' + req.params.pokemon_id);
        PokemonModel.findById(req.params.pokemon_id, function (err, pokemon) {
            if (err) throw err;
            if (pokemon && pokemon.emprunt) {
                pokemon.emprunt = undefined;
                pokemon.save(function () {
                    res.status(204).end();
                });
            } else
                res.status(404).end();
        });



    });

//Route autres (all)
//=============================================================
routerApiEmprunt.route('/pokemons/:pokemon_id/emprunt')
    .all(function (req, res) {
        res.status(405).end();
    });

//exportation de routerAPI
module.exports = routerApiEmprunt;