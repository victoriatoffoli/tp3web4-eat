'use strict';

var express = require('express');

var routerApiUsagers = express.Router();
var url_base = "http://localhost:8090";
//ORM Mongoose
var mongoose = require('mongoose');
// Connexion à MongoDB avec Mongoose
mongoose.connect('mongodb://localhost:27017/web4Tp3', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    poolSize: 10
});

//Importation de modèle Usager
var UsagerModel = require('../models/usagerModels').UsagerModel;

//Importation de modèle Commande
//var CommandeModel = require('../models/commandeModels').CommandeModels;

var jwt = require('jsonwebtoken');

/**
 * Permet de vérifier si l'utilisateur est authentifié à l'aide d'un jeton JWT.
 * @param {Object} req Requête HTTP.
 * @param {Function} callback Fonction de callback.
 */
function verifierAuthentification(req, callback) {
    // Récupération du jeton JWT dans l'en-tête HTTP "Authorization".
    var auth = req.headers.authorization;
    if (!auth) {
        // Pas de jeton donc pas connecté.
        callback(false, null);
    } else {
        // Structure de l'en-tête "Authorization" : "Bearer jeton-jwt"
        var authArray = auth.split(' ');
        if (authArray.length !== 2) {
            // Mauvaise structure pour l'en-tête "Authorization".
            callback(false, null);
        } else {
            // Le jeton est après l'espace suivant "Bearer".
            var jetonEndode = authArray[1];
            // Vérification du jeton.
            jwt.verify(jetonEndode, req.app.get('jwt-secret'), function (err, jetonDecode) {
                if (err) {
                    // Jeton invalide.
                    callback(false, null);
                } else {
                    // Jeton valide.
                    callback(true, jetonDecode);
                }
            });
        }
    }
}


// Création d'un usager
// =======================================================
routerApiUsagers.route('/usagers')
    .post(function (req, res) {
        //on doit créer un usager
        console.log('Création de l\'usager ');
        //création du modèle à partir du body de la requête
        var usager = new UsagerModel(req.body);
        //on sauvegarde dans la BD
        usager.save(function (err) {
            if (err) throw err;

            res.setHeader("Location", "http://localhost:8090/usagers/" + usager.id);
            res.status(201).json(usager);
        });
    });

// Route de consultation d'un usager
// =======================================================
routerApiUsagers.route('/usagers/:usager_id')
    //Consultation d'un usager
    .get(function (req, res) {

        // Vérification de l'authorisation.
        verifierAuthentification(req, function (estAuthentifie, jetonDecode) {
            if (!estAuthentifie) {
                // Utilisateur NON authentifié.
                res.status(401).end();
            } else {
                // Utilisateur authentifié.
                // Sauvegarde du jeton décodé dans la requête pour usage ultérieur.
                req.jeton = jetonDecode;
                // Vérification de l'usager
                
                if (jetonDecode.user === req.params.usager_id ){

                    console.log('consultation de l\'usager no.' + req.params.usager_id);
                    UsagerModel.findById(req.params.usager_id, function (err, usager) {
                        if (err) throw err;
                        if (usager)
                            res.json(usager);
                        else
                            res.status(404).end();
                    });
                   
                }
                else{
                    res.status(403).end();
                }
                
            }
        });

    });

/*
// Route de création d'une commande
// =======================================================
routerApiUsagers.route('/usagers/:usager_id/commandes')
    //Consultation d'un usager
    .post(function (req, res) {

        // Vérification de l'authorisation.
        verifierAuthentification(req, function (estAuthentifie, jetonDecode) {
            if (!estAuthentifie) {
                // Utilisateur NON authentifié.
                res.status(401).end();
            } else {
                // Utilisateur authentifié.
                // Sauvegarde du jeton décodé dans la requête pour usage ultérieur.
                req.jeton = jetonDecode;
                // Vérification de l'usager
                
                if (jetonDecode.user === req.params.usager_id ){

                    //Création de la commande.
                    console.log('Création de la commande ');
                    //création du modèle à partir du body de la requête
                    var commande = new CommandeModel(req.body); //on doit créer un usager
                    //on sauvegarde dans la BD
                    commande.save(function (err) {
                        if (err) throw err;

                        res.setHeader("Location", "http://localhost:8090/usagers/:usager_id/commandes/" + commande.id);
                        res.status(201).json(usager);
                    });
                   
                }
                else{
                    res.status(403).end();
                }
                
            }
        });

    });

*/


//exportation de routerAPI
module.exports = routerApiUsagers;