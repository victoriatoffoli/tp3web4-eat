//ORM Mongoose
var mongoose = require('mongoose');



//création du schéma usager
// pas besoin de spécifier un _id, mongoose le fera pour nous ! 
var usagerShema = new mongoose.Schema({
    nom: {
        type: String,
        required: true
    },
    prenom: {
        type: String,
        required: true
    },
    adresse: {
        type: String,
        required: true
    },
    pseudo: {
        type: String,
        required: true
    },
    motDePasse: {
        type: String,
        required: true
    }

});

// Crée le modèle à partir du schéma et l'Exporte pour pouvoir l'utiliser dans le reste du projet
module.exports.UsagerModel = mongoose.model('Usager',usagerShema);