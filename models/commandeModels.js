//ORM Mongoose
var mongoose = require('mongoose');

var livreurSchema = require('./livreurModels').livreurSchema;
var usagerSchema = require('./usagerModels').usagerSchema;
var platSchema = require('./platModels').platSchema;



//création du schéma commande
// pas besoin de spécifier un _id, mongoose le fera pour nous ! 
var commandeSchema = new mongoose.Schema({
    
    dateRetour: {
        type: Date,
        required:true
    },
    livreur: livreurSchema,
    usager: {
        type: usagerSchema,
        required:true
    },
    plats: {
        type: [platSchema]
    },

});

// Crée le modèle à partir du schéma et l'Exporte pour pouvoir l'utiliser dans le reste du projet
module.exports.CommandeModel = mongoose.model('Commande',commandeSchema);