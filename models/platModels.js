//ORM Mongoose
var mongoose = require('mongoose');

//création du schéma plat
// pas besoin de spécifier un _id, mongoose le fera pour nous ! 
var platSchema = new mongoose.Schema({
    nom: {
        type: String,
        required: true
    },
    nbrPortions: {
        type: Number,
        required: true
    }
});

// Crée le modèle à partir du schéma et l'exporte pour pouvoir l'utiliser dans le reste du projet
module.exports.PlatModel = mongoose.model('Plat',platSchema);